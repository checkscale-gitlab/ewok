Ewok
========

Ewok is a [Continuous Integration][1] and [Delivery][2] showcase built for educational purposes. The application is based on [Symfony Demo Application][3] and was modified to work with automation. The [GitLab Pipeline][4] provides a deployable custom image whenever master branch is modified.


Requirements
--------

The application uses several tools but it's not needed to install them on your machine because they are available into docker containers.

Therefore only docker and makefile are required.

* [Docker][5]
* [Makefile][6]


Installation
--------

To make application available on **staging environment** you need to build docker containers and then install application dependencies. Once you execute these commands you can access http://localhost:8080 address in your browser.

```bash
$ git clone git@gitlab.com:renedelima/ewok.git
$ cd ewok
$ make install
```

Usage
--------

The Makefile was built to help you on application, database and environment management. Beyond that, you can also access the application container through the terminal whenever you need using `sh` target.

```bash
$ make sh
```

Quality Assurance
--------

Quality tools available on development environment are **exactly the same** as those used on integration environment. Execute these commands to perform checks and tests:

```bash
$ make lint-twig
$ make lint-yaml
$ make phpcs
$ make phpunit
```

[1]: https://en.wikipedia.org/wiki/Continuous_integration
[2]: https://en.wikipedia.org/wiki/Continuous_delivery
[3]: https://github.com/symfony/demo
[4]: https://docs.gitlab.com/ee/ci/pipelines.html
[5]: https://www.docker.com
[6]: https://www.gnu.org/software/make/manual/html_node/Introduction.html
